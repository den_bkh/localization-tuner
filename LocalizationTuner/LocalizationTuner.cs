﻿using System;
using System.Reflection;
using Harmony12;
using Kingmaker.EntitySystem;
using Kingmaker.GameModes;
using Kingmaker.Localization;
using Kingmaker.Localization.Shared;
using Kingmaker.PubSubSystem;
using Kingmaker.UI;
using UnityEngine;
using UnityModManagerNet;

namespace LocalizationTuner
{
    static class LocalizationTuner
    {
        //Log file location: %HOMEDRIVE%%HOMEPATH%\AppData\LocalLow\Owlcat Games\Pathfinder Kingmaker\
        private static UnityModManager.ModEntry.ModLogger _logger;
        private static LocalizationTunerSettings _settings;
        private static Locale _secondLanguage = Locale.ruRU;
        public static LocalizationPack SecondLocalizationPack { get; set; }

        static void Load(UnityModManager.ModEntry modEntry)
        {
            _settings = UnityModManager.ModSettings.Load<LocalizationTunerSettings>(modEntry);
            if (!String.IsNullOrEmpty(_settings.SecondLanguage))
            {
                _secondLanguage = (Locale)Enum.Parse(typeof(Locale), _settings.SecondLanguage);
            }

            _logger = modEntry.Logger;
            modEntry.OnGUI = OnGUI;
            modEntry.OnSaveGUI = OnSaveGUI;

            HarmonyInstance harmony = HarmonyInstance.Create(modEntry.Info.Id);
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
        
        static void OnGUI(UnityModManager.ModEntry modEntry)
        {
            GUILayout.Label("Main language: " + 
                            LocaleToLanguage(LocalizationManager.CurrentLocale) + 
                            " (can be changed in the game's settings)");
            
            GUILayout.Label("Language for dialogs and cutscenes:");
            
            //GUILayout.BeginVertical("box");
            foreach (Locale locale in Enum.GetValues(typeof(Locale)))
            {
                if(locale == Locale.Sound) continue;
                if (GUILayout.Toggle(_secondLanguage == locale, LocaleToLanguage(locale)))
                {
                    _secondLanguage = locale;
                }
            }
            //GUILayout.EndVertical();
            GUILayout.Label("After change, press the 'Save' button below and restart the game");
        }

        private static String LocaleToLanguage(Locale locale)
        {
            switch (locale)
            {
                case Locale.enGB:
                    return "English";
                case Locale.ruRU:
                    return "Russian";
                case Locale.deDE:
                    return "German";
                case Locale.frFR:
                    return "French";
                case Locale.itIT:
                    return "Italian";
                case Locale.esES:
                    return "Spanish";
                case Locale.zhCN:
                    return "Chinese";
                default:
                    return locale.ToString();
            }
        }
        
        static void OnSaveGUI(UnityModManager.ModEntry modEntry)
        {
            _settings.SecondLanguage = _secondLanguage.ToString();
            _settings.Save(modEntry);
            _logger.Log("settings saved");
        }

        [HarmonyPatch(typeof(LocalizationManager), "Init")]
        static class LocalizationManagerInitPatch
        {
            private static bool _runOnce = false;
            static void Postfix()
            {
                //I don't have an idea why devs run LocalizationManager.Init() multiple times ...
                if(_runOnce) return;
                _runOnce = true;
                
                MethodInfo loadPack = typeof(LocalizationManager).GetMethod("LoadPack", 
                    BindingFlags.Static | BindingFlags.NonPublic);
                
                SecondLocalizationPack = (LocalizationPack)loadPack.Invoke(null,
                    new object[] {_secondLanguage});
                if (SecondLocalizationPack == null)
                {
                    _logger.Error("LocalizationManager.LoadPack returns null for Locale = " + _secondLanguage);
                    return;
                }

                var result = EventBus.Subscribe(new LocalizationPackChanger());
                _logger.Log("LocalizationPackChanger loaded");
            }
        }

        [HarmonyPatch(typeof(LocalizationManager), "AppendSourcedPack", 
            new Type[]{ typeof(string)})]
        static class AppendSourcedPackPatch
        {
            static void Prefix(ref LocalizationPack __state, string sourceId)
            {
                if (SecondLocalizationPack != null)
                {
                    __state = LocalizationManager.CurrentPack;
                    LocalizationManager.CurrentPack = SecondLocalizationPack;
                }
            }

            static void Postfix(ref LocalizationPack __state, string sourceId)
            {
                if (__state != null)
                {
                    LocalizationManager.CurrentPack = __state;
                }
            }
        }

        [HarmonyPatch(typeof(UIAccess), "Bark", 
            new Type[] { typeof(EntityDataBase), typeof(LocalizedString), typeof(float), typeof(bool) })]
        static class UIAccessBarkPatch
        {
            static bool Prefix(
                ref LocalizationPack __state,
                EntityDataBase entity,
                LocalizedString text,
                float duration,
                bool durationByVoice)
            {
                //_logger.Log("UIAccess.Bark. Key = " + text.Key);
                if (SecondLocalizationPack != null)
                {
                    __state = LocalizationManager.CurrentPack;
                    LocalizationManager.CurrentPack = SecondLocalizationPack;
                }
                
                return true;
            }

            static void Postfix(
                LocalizationPack __state,
                EntityDataBase entity,
                LocalizedString text,
                float duration,
                bool durationByVoice)
            {
                if (__state != null)
                {
                    LocalizationManager.CurrentPack = __state;
                }
            }
        }
    }
    
    public class LocalizationTunerSettings : UnityModManager.ModSettings
    {
        public String SecondLanguage;
        public override void Save(UnityModManager.ModEntry modEntry)
        {
            UnityModManager.ModSettings.Save<LocalizationTunerSettings>(this, modEntry);
        }
    }

    class LocalizationPackChanger : IGameModeHandler
    {
        private LocalizationPack _origPack;

        public void OnGameModeStart(GameModeType gameMode)
        {
            //_logger.Log("OnGameModeStart: " + gameMode);
            if (gameMode == GameModeType.Dialog 
                || gameMode == GameModeType.Cutscene
                || gameMode == GameModeType.CutsceneGlobalMap)
            {
                _origPack = LocalizationManager.CurrentPack;
                LocalizationManager.CurrentPack = LocalizationTuner.SecondLocalizationPack;
            }
            
        }

        public void OnGameModeStop(GameModeType gameMode)
        {
            //_logger.Log("OnGameModeStop: " + gameMode);
            if (gameMode == GameModeType.Dialog 
                || gameMode == GameModeType.Cutscene
                || gameMode == GameModeType.CutsceneGlobalMap)
            {
                LocalizationManager.CurrentPack = _origPack;
            }
        }
    }
    
}