@echo off

set MOD_NAME=LocalizationTuner
set RELEASE_DIR=%~dp0\..\%MOD_NAME%\bin\Release
set GAME_MOD_DIR="J:\Steam\steamapps\common\Pathfinder Kingmaker\Mods\%MOD_NAME%"

del /q %GAME_MOD_DIR%\%MOD_NAME%*
copy /y %RELEASE_DIR%\Info.json %GAME_MOD_DIR%
copy /y %RELEASE_DIR%\%MOD_NAME%.dll %GAME_MOD_DIR%
copy /y %RELEASE_DIR%\%MOD_NAME%.pdb %GAME_MOD_DIR%